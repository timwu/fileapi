FROM node:8.12-jessie

RUN git clone https://gitlab.com/timwu/fileapi.git

WORKDIR /fileapi/

RUN npm install

CMD node app.js